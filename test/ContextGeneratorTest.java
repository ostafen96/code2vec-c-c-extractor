package test;

import code2vec.ast.ASTNode;
import code2vec.ast.ASTPath;
import code2vec.ast.cdt.CDTASTNodeFactory;
import code2vec.Context;
import code2vec.ContextGenerator;
import code2vec.Code2VecException;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.dom.ast.gnu.c.GCCLanguage;
import org.eclipse.cdt.core.parser.*;
import org.eclipse.core.runtime.CoreException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ContextGeneratorTest {
    private static final String SOURCES_DIR = "/home/ostafen/Desktop/out";

    IASTTranslationUnit parseFile(String fileName) {
        FileContent fileContent = FileContent.createForExternalFileLocation(fileName);
        Map definedSymbols = new HashMap();
        String[] includePaths = new String[0];
        IScannerInfo info = new ScannerInfo(definedSymbols, includePaths);
        IParserLogService log = new DefaultLogService();
        IncludeFileContentProvider emptyIncludes = IncludeFileContentProvider.getEmptyFilesProvider();

        int opts = 8;
        IASTTranslationUnit translationUnit = null;
        try {
            return GCCLanguage.getDefault()
                    .getASTTranslationUnit(fileContent, info, emptyIncludes, null, opts, log);
        } catch (CoreException e) {
            //e.printStackTrace();
            return null;
        }
    }

    @org.junit.jupiter.api.Test
    void encode() throws IOException {
        Path path = Paths.get(SOURCES_DIR);

        Files.walk(path)
                .filter(Files::isRegularFile)
                .map(pth -> parseFile(pth.toString()))
                .forEach(tu -> {
                    ASTNode rootNode = CDTASTNodeFactory.fromCDTNode(tu);
                    ContextGenerator generator = ContextGenerator.getInstance();
                    try {
                        List<Context> contexts = generator.fromASTNode(rootNode);

                        contexts.forEach(c -> {
                            ASTNode leftNode = c.getLeftNode();
                            ASTNode rightNode = c.getRightNode();
                            ASTPath leftPath = c.getLeftPath();
                            ASTPath rightPath = c.getRightPath();

                            for(int i = 0; i<leftPath.pathLength(); i++) {
                                assertFalse( leftPath.nodeAt(i).isLeaf() );
                            }

                            for(int i = 0; i<rightPath.pathLength(); i++) {
                                assertFalse( rightPath.nodeAt(i).isLeaf() );
                            }

                            assertTrue(leftNode.isLeaf() && rightNode.isLeaf());
                        });
                    } catch (Code2VecException e) {
                        e.printStackTrace();
                        assertTrue(false);
                    }
                });
        }
}