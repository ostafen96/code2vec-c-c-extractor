# Code2Vec C/C++ Extractor

Code2Vec C/C++ Extractor è un API che fornisce un'implementazione della codifica Code2Vec per codice sorgente in linguaggio C/C++.
Essa fa uso del parser Eclipse CDT, che, a differenza di Clang, permettono di effettuare il parsing anche nel caso di codice incompleti (porzioni di codice che referenziano variabili fuori dal contesto di inizializzazione).
A differenza dell'estractor ufficiale (https://github.com/tech-srl/code2vec), che implementa la codifica soltanto per interi metodi, questa libreria permette di codificare una qualsiasi porzione di codice e può essere estesa per supportare altri linguaggi.

# Requisiti
  - Java >= 8
  - Eclipse CDT

Per compilare il sorgente, è necessario che le dipendenze relative alla libreria Eclipse CDT si trovino all'interno di una cartella denominata "jars".

# Esecuzione

Il jar contiene un main che accetta uno snippet di codice come standard input ed emette un file json nello standard output.

```sh
$ java -jar extractor.jar
usage: Loop2Vec Extractor [-h] [-s]
Extract context from supplied snippet of code.

 -h   Print usage.
 -s   Parse input string.

Authors: Stefano Scafiti
```