import code2vec.*;
import code2vec.ast.ASTNode;
import code2vec.ast.cdt.CDTASTNodeFactory;
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.dom.ast.gnu.c.GCCLanguage;
import org.eclipse.cdt.core.dom.ast.gnu.cpp.GPPLanguage;
import org.eclipse.cdt.core.parser.*;
import org.eclipse.core.runtime.CoreException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {
    public static void main(String args[]) throws IOException {
        if(args.length > 0) {
            Path path = Paths.get(args[0]);

            List<Context> allContexts = new ArrayList<>();

            Files.walk(path).filter(Files::isRegularFile).
                    forEach(file -> {
                IASTTranslationUnit tu = getTranslationUnitFromFile(file.toString());
                ASTNode node = CDTASTNodeFactory.fromCDTNode(tu);
                ContextGenerator generator = ContextGenerator.getInstance();
                try {
                    List<Context> contexts = generator.fromASTNode(node);
                    allContexts.addAll(contexts);
                } catch (Code2VecException e) {
                    e.printStackTrace();
                }
            });


        }
    }

    static IASTTranslationUnit getTranslationUnitFromFile(String filename) {
        FileContent fileContent = FileContent.createForExternalFileLocation(filename);
        Map definedSymbols = new HashMap();
        String[] includePaths = new String[0];
        IScannerInfo info = new ScannerInfo(definedSymbols, includePaths);
        IParserLogService log = new DefaultLogService();
        IncludeFileContentProvider emptyIncludes = IncludeFileContentProvider.getEmptyFilesProvider();

        int opts = 8;
        IASTTranslationUnit translationUnit = null;
        try {
            return GPPLanguage.getDefault()
                    .getASTTranslationUnit(fileContent, info, emptyIncludes, null, opts, log);
        } catch (CoreException e) {
            //e.printStackTrace();
            return null;
        }
    }
}
