import code2vec.*;
import code2vec.ast.ASTNode;
import code2vec.ast.cdt.CDTASTNodeFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import loop.ContextSerializer;
import loop.ForRecord;
import loop.ForStatementExtractor;
import org.apache.commons.cli.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.eclipse.cdt.core.dom.ast.*;
import org.eclipse.cdt.core.dom.ast.gnu.cpp.GPPLanguage;
import org.eclipse.cdt.core.parser.*;
import org.eclipse.core.runtime.CoreException;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

class Main {
    private static final int DEFAULT_MAX_CONTEXTS = 200;

    private static void printUsageAndExit(Options opts) {
        HelpFormatter formatter = new HelpFormatter();
        String header = "Extract context from supplied for loops.\n\n";
        String footer = "\nAuthors: Stefano Scafiti, Giovanni Pasqualino";
        formatter.printHelp("Loop2Vec Extractor", header, opts, footer, true);
        System.exit(1);
    }

    public static void main(String[] args) throws IOException {
        Options opts = new Options();

        opts.addOption("s", false, "Parse input string.");
        opts.addOption("h", false, "Print usage." );

        if(args.length == 0)
            printUsageAndExit(opts);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse( opts, args);
        } catch (ParseException e) {
            printErrorAndExit(e.getMessage());
        }

        if(cmd.hasOption("h")) printUsageAndExit(opts);

        if(cmd.hasOption("s")) {
            Scanner sc = new Scanner(System.in);
            String forStmt = sc.nextLine();

            List<Context> contexts = extractContexts(forStmt);

            if(contexts != null) {
                List<ContextSerializer> serializers = contexts.stream()
                        .map(c -> new ContextSerializer(c))
                        .collect(Collectors.toList());

                ForRecord record = new ForRecord(serializers, contexts, false);
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                String jsonObj = gson.toJson(record);
                System.out.println(jsonObj);
            } else {
                System.exit(1);
            }
        }

        else {
            Path path = Paths.get(args[0]);

            List<ForRecord> records = new ArrayList<>();
            List<Context> allContexts = new ArrayList<>();

            try {
                Reader reader = Files.newBufferedReader(path);
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                        .withFirstRecordAsHeader());

                for (CSVRecord csvRecord : csvParser) {
                    String forStmt = csvRecord.get(0);
                    boolean isParallel = Integer.parseInt( csvRecord.get(1) ) == 1;

                    List<Context> contexts = extractContexts(forStmt);

                    if(contexts != null) {
                        List<ContextSerializer> serializers = contexts.stream()
                                .map(c -> new ContextSerializer(c))
                                .collect(Collectors.toList());

                        ForRecord record = new ForRecord(serializers, contexts, isParallel);
                        records.add(record);
                        allContexts.addAll(contexts);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            ContextHistogram contextHistogram = ContextHistogram.compute(allContexts);
            ContextSampler sampler = new ContextSampler(contextHistogram);

            for(ForRecord rec : records) {
                List<Context> contexts = rec.getContexts();
                List<Context> ctxSamples = sampler.sample(contexts, DEFAULT_MAX_CONTEXTS);

                List<ContextSerializer> ser = ctxSamples.
                        stream()
                        .map(c -> new ContextSerializer(c))
                        .collect(Collectors.toList());

                rec.setSerializers(ser);
            }

            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            String jsonObj = gson.toJson(records);
            FileWriter writer = new FileWriter("data.json");
            writer.write(jsonObj);
            writer.close();
        }
    }

    private static void printErrorAndExit(String message) {
        System.out.println("Error: " + message);
        System.exit(1);
    }

    private static String wrapInsideFunction(String stmt) {
        return "void f() {" + stmt + "}";
    }

    private static List<Context> extractContexts(String forStmt) {
        String code = wrapInsideFunction(forStmt);
        IASTTranslationUnit tu = getTranslationUnitFromString(code);
        ForStatementExtractor extractor = new ForStatementExtractor();
        tu.accept(extractor);

        List<IASTForStatement> forStatements = extractor.getResults();

        if(forStatements.isEmpty()) { //skip this
            return null;
        }

        IASTForStatement stmt = forStatements.get(0);
        ASTNode rootNode = CDTASTNodeFactory.fromCDTNode(stmt);
        ContextGenerator ctxGenerator = ContextGenerator.getInstance();
        try {
            return ctxGenerator.fromASTNode(rootNode);
        } catch (Code2VecException e) {
            e.printStackTrace();
            return null;
        }
    }

    static IASTTranslationUnit getTranslationUnitFromString(String code) {
        FileContent fileContent = FileContent.create("<test-code>", code.toCharArray());
        Map definedSymbols = new HashMap();
        String[] includePaths = new String[0];
        IScannerInfo info = new ScannerInfo(definedSymbols, includePaths);
        IParserLogService log = new DefaultLogService();
        IncludeFileContentProvider emptyIncludes = IncludeFileContentProvider.getEmptyFilesProvider();

        int opts = 8;
        IASTTranslationUnit translationUnit = null;
        try {
            return GPPLanguage.getDefault()
                    .getASTTranslationUnit(fileContent, info, emptyIncludes, null, opts, log);
        } catch (CoreException e) {
            //e.printStackTrace();
            return null;
        }
    }
}
