package loop;

import code2vec.Context;

public class ContextSerializer {
    private String left, right, path;

    public ContextSerializer(Context c) {
        this.left = c.getLeftNode().toString();
        this.right = c.getRightNode().toString();
        this.path = c.getEncodedPath(true);
    }
}
