package loop;

import code2vec.Context;

import java.util.List;

public class ForRecord {
    private boolean isParallel;
    private transient List<Context> contexts;
    private List<ContextSerializer> serializers;

    public ForRecord(List<ContextSerializer> serializers, List<Context> contexts, boolean isParallel) {
        this.isParallel = isParallel;
        this.serializers = serializers;
        this.contexts = contexts;
    }

    public List<Context> getContexts() {
        return contexts;
    }

    public void setSerializers(List<ContextSerializer> ser) {
        this.serializers = ser;
    }
}
