package loop;

import org.eclipse.cdt.core.dom.ast.ASTVisitor;
import org.eclipse.cdt.core.dom.ast.IASTForStatement;
import org.eclipse.cdt.core.dom.ast.IASTStatement;

import java.util.ArrayList;
import java.util.List;

public class ForStatementExtractor extends ASTVisitor {
    private List<IASTForStatement> forStatements = new ArrayList<>();

    public ForStatementExtractor() {
        this.shouldVisitStatements = true;
    }

    public int visit(IASTStatement stmt) {
        if(stmt instanceof IASTForStatement) {
            IASTForStatement forStatement = (IASTForStatement) stmt;
            forStatements.add(forStatement);
        }
        return ASTVisitor.PROCESS_CONTINUE;
    }

    public List<IASTForStatement> getResults() {
        return forStatements;
    }
}
