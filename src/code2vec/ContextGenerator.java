package code2vec;

import code2vec.ast.ASTNode;
import code2vec.ast.ASTPath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//Domanda: inserire nel path anche i nodi terminali? code2vec-c fa così

public class ContextGenerator {
    private static ContextGenerator instance = new ContextGenerator();

    private ContextGenerator() {}

    public static ContextGenerator getInstance() {
        return instance;
    }

    public List<Context> fromASTNode(ASTNode node) throws Code2VecException {
        List<ASTNode> leaves = node.getAllLeaves();

        if(leaves.size() <= 1) return Collections.emptyList();

        List<Context> contexts = new ArrayList<>();

        for(int l = 0; l<leaves.size()-1; l++) {
            for(int r = l+1; r<leaves.size(); r++) {
                ASTNode leftNode = leaves.get(l);
                ASTNode rightNode = leaves.get(r);

                ASTPath leftPath = ASTPath.getPathToRoot(leftNode);
                ASTPath rightPath = ASTPath.getPathToRoot(rightNode);

                ASTNode ancestor = ASTPath.nearestCommonAncestor(leftPath, rightPath);

                leftPath.prunePathToAncestor(ancestor);
                rightPath.prunePathToAncestor(ancestor);
                rightPath.reverse();

                leftPath.discardHead();
                rightPath.discardTail();

                Context c = new Context();
                c.setLeftNode(leftNode);
                c.setRightNode(rightNode);
                c.setAscendingPath(leftPath);
                c.setDescendingPath(rightPath);
                c.setAncestor(ancestor);

                contexts.add(c);
            }
        }
        return contexts;
    }

}
