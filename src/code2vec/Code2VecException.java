package code2vec;

public class Code2VecException extends Exception {
    public Code2VecException(String message) {
        super(message);
    }
}
