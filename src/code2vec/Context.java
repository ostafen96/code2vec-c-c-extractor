package code2vec;

import code2vec.ast.ASTNode;
import code2vec.ast.ASTPath;
import code2vec.util.HashUtils;
import java.util.stream.Collectors;

public class Context {
    private ASTNode leftNode, rightNode;
    private ASTPath leftPath, rightPath;
    private ASTNode ancestor;

    public void setAscendingPath(ASTPath leftPath) {
        this.leftPath = leftPath;
    }

    public void setDescendingPath(ASTPath rightPath) {
        this.rightPath = rightPath;
    }

    public void setAncestor(ASTNode node) {
        this.ancestor = node;
    }

    public ASTPath getLeftPath() {
        return leftPath;
    }

    public ASTPath getRightPath() {
        return rightPath;
    }

    public void setLeftNode(ASTNode leftNode) {
        this.leftNode = leftNode;
    }

    public void setRightNode(ASTNode rightNode) {
        this.rightNode = rightNode;
    }

    public ASTNode getLeftNode() {
        return leftNode;
    }

    public ASTNode getRightNode() {
        return rightNode;
    }

    public String encode(boolean hashPath) {
        return "(" + leftNode + "," + getEncodedPath(hashPath) + "," + rightNode+")";
    }

    public String getEncodedPath(boolean hash) {
        String leftPathStr = leftPath.toList().stream()
                .map(node -> "(" + node.toString() + ")")
                .collect(Collectors.joining("^"));

        String rightPathStr = rightPath.toList().stream().
                map(node -> "(" + node.toString() + ")")
                .collect(Collectors.joining("_"));

        String path = "(" + ancestor + ")";

        if (!leftPathStr.isEmpty())
            path = leftPathStr + "^" + path;

        if (!rightPathStr.isEmpty())
            path = path + "_" + rightPathStr;

        if (hash)
            path = HashUtils.sha256Hash(path);

        return path;
    }
}