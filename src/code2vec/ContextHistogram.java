package code2vec;

import java.util.HashMap;
import java.util.List;

public class ContextHistogram {
    private HashMap<String, Integer> wordCount = new HashMap<>();
    private HashMap<String, Integer> pathCount = new HashMap<>();

    public static ContextHistogram compute(List<Context> contexts) {
        ContextHistogram histo = new ContextHistogram();

        for(Context ctx : contexts) {
            String path = ctx.getEncodedPath(true);
            String leftNode = ctx.getLeftNode().toString();
            String rightNode = ctx.getRightNode().toString();

            histo.wordCount.putIfAbsent(leftNode, 1);
            histo.wordCount.putIfAbsent(rightNode, 1);
            histo.pathCount.putIfAbsent(path, 1);

            histo.wordCount.computeIfPresent(leftNode, (k, c) -> c + 1);
            histo.wordCount.computeIfPresent(rightNode, (k, c) -> c + 1);
            histo.pathCount.computeIfPresent(path, (k, c) -> c + 1);
        }

        return histo;
    }

    public int getCountForWord(String word) {
        return wordCount.getOrDefault(word, 0);
    }

    public int getCountForPath(String path) {
        return pathCount.getOrDefault(path, 0);
    }

    public boolean wordIsPresent(String word) {
        return wordCount.containsKey(word);
    }

    public boolean pathIsPresent(String path) {
        return pathCount.containsKey(path);
    }
}
