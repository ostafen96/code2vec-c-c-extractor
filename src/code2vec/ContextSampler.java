package code2vec;

import code2vec.util.ListUtils;

import java.util.List;
import java.util.stream.Collectors;

public class ContextSampler {
    ContextHistogram histogram;

    public ContextSampler(ContextHistogram histogram) {
        this.histogram = histogram;
    }

    private boolean contextFullFound(Context context) {
        String leftWord = context.getLeftNode().toString();
        String rightWord = context.getRightNode().toString();
        String path = context.getEncodedPath(true);

        return histogram.wordIsPresent(leftWord) &&
                histogram.wordIsPresent(rightWord) &&
                histogram.pathIsPresent(path);
    }

    private boolean contextPartialFound(Context context) {
        String leftWord = context.getLeftNode().toString();
        String rightWord = context.getRightNode().toString();
        String path = context.getEncodedPath(true);

        return histogram.wordIsPresent(leftWord) ||
                histogram.wordIsPresent(rightWord) ||
                histogram.pathIsPresent(path);
    }

    public List<Context> sample(List<Context> contexts, int numSamples) {
        if(contexts.size() <= numSamples) {
            return contexts;
        }

        List<Context> fullFoundContexts = contexts.stream()
                .filter(this::contextFullFound)
                .collect(Collectors.toList());

        List<Context> partialFoundContexts = contexts.stream()
                .filter(this::contextPartialFound)
                .collect(Collectors.toList());

        if(fullFoundContexts.size() > numSamples)
            return ListUtils.sample(fullFoundContexts, numSamples);

        return ListUtils.concatenate(fullFoundContexts,
                ListUtils.sample(partialFoundContexts, numSamples-fullFoundContexts.size()));
    }
}
