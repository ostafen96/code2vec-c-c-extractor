package code2vec.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListUtils {

    public static <T> List<T> sample(List<T> inputList, int numSamples) {
        if(inputList.size() < numSamples) return inputList;
        Collections.shuffle(inputList);
        return inputList.subList(0, numSamples);
    }

    public static <T> List<T> concatenate(List<T> l1, List<T> l2) {
        List<T> res = new ArrayList<>();
        res.addAll(l1);
        res.addAll(l2);
        return res;
    }
}
