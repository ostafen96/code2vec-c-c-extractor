package code2vec.ast;

import java.util.ArrayList;
import java.util.List;

public class ASTNode {
    private ASTNode parent;
    private List<ASTNode> children = new ArrayList<>();
    private String type;
    private int depth = -1;
    boolean isLeaf = false;

    public void setParent(ASTNode p) {
        parent = p;
    }

    public void addChild(ASTNode child) {
        children.add(child);
    }

    public ASTNode getParent() {
        return parent;
    }

    public List<ASTNode> getChildren() {
        return children;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public List<ASTNode> getAllLeaves() {
        List<ASTNode> leaves = new ArrayList<>();

        if(isLeaf) {
            leaves.add(this);
            return leaves;
        }

        children.stream().forEach(child -> leaves.addAll(child.getAllLeaves()));

        return leaves;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    @Override
    public String toString() {
        return type;
    }

    public void setNodeType(String nodeType) {
        this.type = nodeType;
    }

    public ASTNode find(ASTNode node) {
        if(node.equals(this))
            return node;

        for(ASTNode child : children) {
            ASTNode result = child.find(node);
            if(result != null) return result;
        }

        return null;
    }

    public void setLeaf(boolean isLeaf) {
        this.isLeaf = isLeaf;
    }
}
