package code2vec.ast;

public interface ASTParser {
    ASTNode parse(String fileName);
}
