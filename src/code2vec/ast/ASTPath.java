package code2vec.ast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ASTPath {
    private List<ASTNode> path = new ArrayList<>();

    public ASTNode head() {
        return path.get(0);
    }

    public int pathLength() {
        return path.size();
    }

    public void appendNode(ASTNode node) {
        path.add(node);
    }

    public static ASTPath getPathToRoot(ASTNode node) {
        ASTPath path = new ASTPath();

        ASTNode current = node;
        while(current != null) {
            path.appendNode(current);
            current = current.getParent();
        }
        return path;
    }

    public static ASTNode nearestCommonAncestor(ASTPath p1, ASTPath p2) {
        ASTNode leftLeaf = p1.head();
        ASTNode rightLeaf = p2.head();

        ASTNode ancestor = leftLeaf.getParent();

        if(ancestor == null) return leftLeaf; //ancestor equals rootNode

        while (ancestor.getParent() != null) {

            if(ancestor.find(rightLeaf) != null)
                return ancestor;

            ancestor = ancestor.getParent();
        }
        return ancestor;
    }

    public void prunePathToAncestor(ASTNode ancestor) {
        for(int i = 0; i<path.size(); i++) {
            if(path.get(i).equals(ancestor)) {
                path = path.subList(0, i);
                return;
            }
        }
    }

    public static ASTPath concatenate(ASTPath p1, ASTPath p2) {
        ASTPath p = p1.copy();
        for(ASTNode node : p2.path)
            p.appendNode(node);
        return p;
    }

    public ASTPath copy() {
        ASTPath copy = new ASTPath();
        for(ASTNode node : path) {
            copy.appendNode(node);
        }
        return copy;
    }

    public void reverse() {
        Collections.reverse(path);
    }

    public void discardTerminalNodes() {
        path = path.subList(1, path.size()-1);
    }

    public List<ASTNode> toList() {
        List<ASTNode> pathList = new ArrayList<>();
        pathList.addAll(path);
        return pathList;
    }

    public void discardHead() {
        path = path.subList(1, path.size());
    }

    public void discardTail() {
        path = path.subList(0, path.size()-1);
    }

    public ASTNode nodeAt(int i) {
        return path.get(i);
    }
}
